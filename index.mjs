import util from 'util';

import * as utils from 'test-utils';

const params = { password: 'df345w43jkj3443d' };

const promiceFunction = util.promisify(utils.runMePlease);

try {
    const result = await promiceFunction(params);
    console.log(result);
} catch (error) {
    console.log(error);
}
